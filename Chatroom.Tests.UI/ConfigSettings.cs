﻿namespace Chatroom.Tests.UI
{
    public class ConfigSettings
    {
        public string AppUrl;
        public string ApiUrl;
        public string WsUrl;
        public int WaitTimeout;
    }
}
