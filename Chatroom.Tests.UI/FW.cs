﻿using Chatroom.Tests.UI.Helpers;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;

namespace Chatroom.Tests.UI
{
    public class FW
    {
        private static bool _testResultsCleaned;

        public static Logger Log => _logger ?? throw new NullReferenceException("_logger is null. Set logger first");
        public static ConfigSettings Config => _configuration ?? throw new NullReferenceException("_configuration is null. Set configuration first");

        [ThreadStatic]
        public static DirectoryInfo CurrentTestDirectory;

        [ThreadStatic]
        private static Logger _logger;

        private static ConfigSettings _configuration;

        public static DirectoryInfo CreateTestResultsDirectory()
        {
            var testDirectory = "TestResults";

            if (Directory.Exists(testDirectory) && !_testResultsCleaned)
            {
                _testResultsCleaned = true;
                Directory.Delete(testDirectory, true);
            }

            return Directory.CreateDirectory(testDirectory);
        }

        public static void SetLogger()
        {
            lock (SetLoggerLock)
            {
                var testResultsDir = "TestResults";
                var testName = TestContext.CurrentContext.Test.Name;
                var testClass = TestContext.CurrentContext.Test.ClassName.Split('.').Last();
                var fullPath = $"{testResultsDir}/{testClass}/{testName}";

                CurrentTestDirectory = Directory.Exists(fullPath) ? Directory.CreateDirectory(fullPath + TestContext.CurrentContext.Test.ID) : Directory.CreateDirectory(fullPath);

                _logger = new Logger(testName, $"{CurrentTestDirectory.FullName}/log.txt");
            }
        }

        public static void LoadConfiguration()
        {
            if (_configuration == null)
            {
                var json = File.ReadAllText("settings.json");
                _configuration = JsonConvert.DeserializeObject<ConfigSettings>(json);
            }
        }

        private static readonly object SetLoggerLock = new object();
    }
}
