﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Models;
using Newtonsoft.Json;
using OpenQA.Selenium.Support.Extensions;
using System.Collections.Generic;

namespace Chatroom.Tests.UI.Services
{
    public class LocalStorageService
    {
        private const string ChatsStorageItemName = "chats";

        public void AddConnectionInBrowser(Connection connection)
        {
            var list = new List<Connection>() { connection };
            SetItem(JsonConvert.SerializeObject(list));
        }

        public void DeleteConnectionsInBrowser()
        {
            Driver.Instance.ExecuteJavaScript($"localStorage.setItem('{ChatsStorageItemName}','')");
        }

        private void SetItem(string value)
        {
            Driver.Instance.ExecuteJavaScript($"localStorage.setItem('{ChatsStorageItemName}','{value}')");
        }
    }
}
