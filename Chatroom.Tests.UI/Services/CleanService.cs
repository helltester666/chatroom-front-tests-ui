﻿using Chatroom.Tests.UI.Models;

namespace Chatroom.Tests.UI.Services
{
    public class CleanService
    {
        public void DeleteChatRoom(ChatRoom chat)
        {
            HelpServices.ApiService.DeleteChat(chat);
        }
    }
}
