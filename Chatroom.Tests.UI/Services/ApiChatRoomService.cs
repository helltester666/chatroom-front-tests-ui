﻿using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.Models;
using RestSharp;
using System;
using System.Net;

namespace Chatroom.Tests.UI.Services
{
    public class ApiChatRoomService
    {
        private static readonly string ChatRoomApi = FW.Config.ApiUrl;
        private readonly RestClient _client = new RestClient(ChatRoomApi);

        public void CreateChat(ChatRoom chat)
        {
            FW.Log.Step($"Creating chat room with name: {chat.Name}", StepType.API);

            var request = new RestRequest("chat");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(chat);

            var response = _client.Post<ChatRoom>(request);
            if (!response.IsSuccessful)
            {
                throw new Exception($"Chat room endpoint failed with {response.StatusCode}");
            }

            chat.Id = response.Data.Id;
            chat.Uuid = response.Data.Uuid;
            chat.Status = response.Data.Status;
        }
        public void ConnectUserToRoom(User user, int chatId)
        {
            FW.Log.Step($"Connecting user {user.Name} to chat with id = {chatId}", StepType.API);

            var request = new RestRequest("chat/join");
            request.RequestFormat = DataFormat.Json;
            request.AddQueryParameter("id", chatId.ToString());
            request.AddJsonBody(user);

            var response = _client.Post<ChatRoom>(request);
            if (!response.IsSuccessful)
            {
                throw new Exception($"Chat/join endpoint failed with {response.StatusCode}");
            }
        }

        public void DeleteChat(ChatRoom chat)
        {
            FW.Log.Step($"Deleting chat room with name: {chat.Name}", StepType.API);

            var request = new RestRequest("chat");
            request.AddQueryParameter("id", chat.Id.ToString());

            var response = _client.Delete(request);
            if (!response.IsSuccessful && response.StatusCode != HttpStatusCode.NotFound)
            {
                throw new Exception($"Chat room endpoint failed with {response.StatusCode}");
            }
        }

        public void EndChat(ChatRoom chat)
        {
            FW.Log.Step($"Ending chat room with name: {chat.Name}", StepType.API);

            var request = new RestRequest("chat");
            request.AddQueryParameter("id", chat.Id.ToString());

            var response = _client.Put<ChatRoom>(request);
            if (!response.IsSuccessful)
            {
                throw new Exception($"Chat room endpoint failed with {response.StatusCode}");
            }
        }
    }
}
