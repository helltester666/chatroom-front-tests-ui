﻿using Chatroom.Tests.UI.Models;
using Microsoft.AspNetCore.SignalR.Client;

namespace Chatroom.Tests.UI.Services
{
    public class WsService
    {
        private static readonly string WsUrl = FW.Config.WsUrl;

        public async void SendMessage(Message message)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl(WsUrl)
                .WithAutomaticReconnect()
                .Build();
            connection.StartAsync().Wait();

            connection.InvokeAsync("SendMessage", message).Wait();

            connection.StopAsync().Wait();
        }
    }
}
