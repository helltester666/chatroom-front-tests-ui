﻿namespace Chatroom.Tests.UI.Services
{
    public class HelpServices
    {
        public static ApiChatRoomService ApiService = new ApiChatRoomService();
        public static AppUrlService Navigation = new AppUrlService();
        public static WsService Socket = new WsService();
        public static CleanService Eraser = new CleanService();
        public static LocalStorageService WebStorage = new LocalStorageService();
    }
}
