﻿namespace Chatroom.Tests.UI.Services
{
    public class AppUrlService
    {
        private readonly string _baseUrl = FW.Config.AppUrl;

        public string GetJoinChatPageUrl(int id)
        {
            return $"{_baseUrl}/join?id={id}";
        }

        public string GetChatRoomPageUrl(int id)
        {
            return $"{_baseUrl}/room?id={id}";
        }

        public string GetHomePageUrl()
        {
            return $"{_baseUrl}";
        }
    }
}
