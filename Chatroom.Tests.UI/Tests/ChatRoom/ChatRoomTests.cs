﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.Models;
using Chatroom.Tests.UI.PageComponents;
using Chatroom.Tests.UI.Services;
using NUnit.Framework;
using Connection = Chatroom.Tests.UI.Models.Connection;

namespace Chatroom.Tests.UI.Tests.ChatRoom
{
    public class ChatRoomTests : TestBase
    {
        private Models.ChatRoom _chat;
        private User _user;
        private Connection _connection;

        [SetUp]
        public void CreateChatRoom()
        {
            _chat = new Models.ChatRoom(TextGenerator.GetRandomUuid());
            _user = new User(TextGenerator.GetRandomName());

            HelpServices.ApiService.CreateChat(_chat);
            HelpServices.ApiService.ConnectUserToRoom(_user, _chat.Id);

            _connection = new Connection(_user, _chat);
            HelpServices.WebStorage.AddConnectionInBrowser(_connection);
        }

        [TearDown]
        public void DeleteChatRoom()
        {
            HelpServices.Eraser.DeleteChatRoom(_chat);
        }

        [Test]
        public void JumpStraightToEndedRoomIfAlreadyJoined()
        {
            HelpServices.ApiService.EndChat(_chat);

            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));

            Assert.True(Components.Shared.Modal.IsDeleteChatModalVisible());
        }

        [Test]
        public void SendMessageAfterJoiningChatRoom()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));
            Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}");

            var message = "Hello, guys!";
            Components.Shared.SubmitForm.SendMessage(message);

            Assert.True(Components.ChatFeed.IsMessagePresent(message));
        }

        [Test]
        public void CanEndChatRoomWithMessage()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));
            Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}");
            Components.Shared.SubmitForm.SendMessage("/end");

            Assert.True(Components.Shared.Modal.IsDeleteChatModalVisible());
        }

        [Test]
        public void CanDeleteRoomAfterItsEnded()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));
            Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}");
            Components.Shared.SubmitForm.SendMessage("/end");
            Components.Shared.Modal.DeleteChat();

            Assert.True(Driver.Wait.Until(d => d.Url.Contains(HelpServices.Navigation.GetHomePageUrl())));
        }

        [Test]
        public void CanReceiveMessageFromOthers()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));
            Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}");

            var anotherUser = new User(TextGenerator.GetRandomName());
            HelpServices.ApiService.ConnectUserToRoom(anotherUser, _chat.Id);

            var text = "Receive me, pls!";
            var message = new Message(_chat, anotherUser, text);
            HelpServices.Socket.SendMessage(message);

            Assert.True(Driver.Wait.Until(d => Components.ChatFeed.IsMessagePresent(text)));
        }

        [Test]
        public void ChatEndedModalShowsWhenSomeoneEndedTheChatRoom()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));
            Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}");

            var anotherUser = new User(TextGenerator.GetRandomName());
            HelpServices.ApiService.ConnectUserToRoom(anotherUser, _chat.Id);

            var message = new Message(_chat, anotherUser, "/end");
            HelpServices.Socket.SendMessage(message);

            Assert.True(Driver.Wait.Until(d => Components.Shared.Modal.IsDeleteChatModalVisible()));
        }

        [Test]
        public void CanJumpStraightToRoomIfAlreadyJoined()
        {
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));

            Assert.True(Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}"));
        }

        [Test]
        public void CannotJumpToRoomImmediately()
        {
            HelpServices.WebStorage.DeleteConnectionsInBrowser();
            Driver.Goto(HelpServices.Navigation.GetChatRoomPageUrl(_chat.Id));

            Assert.True(Components.Shared.Title.WaitForContainsText("Chat for people"));
        }
    }
}
