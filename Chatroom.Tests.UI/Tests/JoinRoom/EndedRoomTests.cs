﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.PageComponents;
using Chatroom.Tests.UI.Services;
using NUnit.Framework;

namespace Chatroom.Tests.UI.Tests.JoinRoom
{
    public class EndedRoomTests : TestBase
    {
        private Models.ChatRoom _chat;

        [SetUp]
        public void CreateChatRoom()
        {
            _chat = new Models.ChatRoom(TextGenerator.GetRandomUuid());

            HelpServices.ApiService.CreateChat(_chat);
        }

        [TearDown]
        public void DeleteChatRoom()
        {
            HelpServices.Eraser.DeleteChatRoom(_chat);
        }

        [Test]
        public void ModalIsShownForEndedRoom()
        {
            HelpServices.ApiService.EndChat(_chat);
            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));

            Assert.True(Components.Shared.Modal.IsDeleteChatModalVisible());
        }

        [Test]
        public void CanDeleteRoomAfterJoinAnEndedRoom()
        {
            HelpServices.ApiService.EndChat(_chat);
            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));
            Components.Shared.Modal.DeleteChat();

            Assert.True(Components.Shared.Title.WaitForContainsText("Chat for people"));
        }
    }
}
