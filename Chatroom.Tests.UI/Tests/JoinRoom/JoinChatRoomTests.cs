﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.Models;
using Chatroom.Tests.UI.PageComponents;
using Chatroom.Tests.UI.Services;
using NUnit.Framework;

namespace Chatroom.Tests.UI.Tests.JoinRoom
{
    public class JoinChatRoomTests : TestBase
    {
        private Models.ChatRoom _chat;
        private User _user;

        [SetUp]
        public void CreateChatRoom()
        {
            _chat = new Models.ChatRoom(TextGenerator.GetRandomUuid());
            _user = new User(TextGenerator.GetRandomName());

            HelpServices.ApiService.CreateChat(_chat);
        }

        [TearDown]
        public void DeleteChatRoom()
        {
            HelpServices.Eraser.DeleteChatRoom(_chat);
        }

        [Test]
        public void JoinChatRoomWithUniqueName()
        {
            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));
            Components.Shared.SubmitForm.JoinChat(_user);

            Assert.True(Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}"));
        }

        [Test]
        public void CannotJoinChatRoomWithNonUniqueName()
        {
            HelpServices.ApiService.ConnectUserToRoom(_user, _chat.Id);

            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));
            Components.Shared.SubmitForm.JoinChat(_user);

            Assert.True(Components.Shared.SubmitForm.ErrorIsVisible());
        }

        [Test]
        public void DismissErrorOnCloseButton()
        {
            HelpServices.ApiService.ConnectUserToRoom(_user, _chat.Id);

            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));
            Components.Shared.SubmitForm.JoinChat(_user);
            Components.Shared.SubmitForm.CloseMessage();

            Assert.True(Components.Shared.SubmitForm.ErrorIsNotVisible());
        }

        [Test]
        public void DismissErrorOnEditText()
        {
            HelpServices.ApiService.ConnectUserToRoom(_user, _chat.Id);

            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));
            Components.Shared.SubmitForm.JoinChat(_user);
            Components.Shared.SubmitForm.FillSubmitFormTextBox("edited");

            Assert.True(Components.Shared.SubmitForm.ErrorIsNotVisible());
        }

        [Test]
        public void JoinChatRoomWithNameFromAnotherChatRoom()
        {
            // Join the first chat room
            HelpServices.ApiService.ConnectUserToRoom(_user, _chat.Id);

            // Create second chat room
            var _anotherChat = new Models.ChatRoom(TextGenerator.GetRandomUuid());
            HelpServices.ApiService.CreateChat(_anotherChat);

            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_anotherChat.Id));
            Components.Shared.SubmitForm.JoinChat(_user);

            Assert.True(Components.Shared.Title.WaitForContainsText($"Hello, {_user.Name}"));

            // Clear data after a test. It's here because of Nunit architecture
            HelpServices.Eraser.DeleteChatRoom(_anotherChat);
        }
    }
}
