﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.PageComponents;
using Chatroom.Tests.UI.Services;
using NUnit.Framework;

namespace Chatroom.Tests.UI.Tests.JoinRoom
{
    public class DeletedRoomTests : TestBase
    {
        private Models.ChatRoom _chat;

        [SetUp]
        public void CreateChatRoom()
        {
            _chat = new Models.ChatRoom(TextGenerator.GetRandomUuid());

            HelpServices.ApiService.CreateChat(_chat);
            HelpServices.ApiService.DeleteChat(_chat);
        }

        [Test]
        public void JoinDeletedRoom()
        {
            Driver.Goto(HelpServices.Navigation.GetJoinChatPageUrl(_chat.Id));

            // It return to main page in application - it's bug
            Assert.True(Components.Shared.Title.WaitForContainsText("Not found"));
        }
    }
}
