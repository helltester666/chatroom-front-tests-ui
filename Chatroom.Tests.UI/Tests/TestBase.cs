﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.PageComponents;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace Chatroom.Tests.UI.Tests
{
    public class TestBase
    {
        [OneTimeSetUp]
        public static void BeforeAll()
        {
            FW.CreateTestResultsDirectory();
            FW.LoadConfiguration();
        }

        [SetUp]
        public static void BeforeEach()
        {
            FW.SetLogger();
            Driver.Init();
            Driver.Instance.Manage().Window.Maximize();
            Driver.Goto(FW.Config.AppUrl);
            Components.Init();
        }

        [TearDown]
        public static void AfterEach()
        {
            var outcome = TestContext.CurrentContext.Result.Outcome.Status;

            if (outcome == TestStatus.Failed)
            {
                Driver.TakeScreenshot("failed");
            }

            FW.Log.Info($"Test finished with status: {outcome.ToString()}");

            Driver.Quit();
        }
    }
}
