﻿using Chatroom.Tests.UI.Helpers;
using Chatroom.Tests.UI.PageComponents;
using Chatroom.Tests.UI.Services;
using NUnit.Framework;

namespace Chatroom.Tests.UI.Tests.CreateRoom
{
    public class CreateChatRoomTests : TestBase
    {
        private Models.ChatRoom _chat;

        [SetUp]
        public void CreateChatRoom()
        {
            _chat = new Models.ChatRoom(TextGenerator.GetRandomUuid());
        }

        [TearDown]
        public void DeleteChatRoom()
        {
            if (_chat.Id != 0)
                HelpServices.Eraser.DeleteChatRoom(_chat);
        }

        [Test]
        public void CreateChatRoomWithUniqueName()
        {
            Components.Shared.SubmitForm.CreateChatRoom(_chat);

            Assert.True(Components.Shared.Modal.IsJoinRoomModalVisible());
        }

        [Test]
        public void CannotCreateChatRoomWithNonUniqueName()
        {
            HelpServices.ApiService.CreateChat(_chat);
            Components.Shared.SubmitForm.CreateChatRoom(_chat);

            Assert.True(Components.Shared.SubmitForm.ErrorIsVisible());
        }

        [Test]
        public void DismissErrorOnCloseButton()
        {
            HelpServices.ApiService.CreateChat(_chat);
            Components.Shared.SubmitForm.CreateChatRoom(_chat);
            Components.Shared.SubmitForm.CloseMessage();

            Assert.True(Components.Shared.SubmitForm.ErrorIsNotVisible());
        }

        [Test]
        public void DismissErrorOnEnterText()
        {
            HelpServices.ApiService.CreateChat(_chat);
            Components.Shared.SubmitForm.CreateChatRoom(_chat);
            Components.Shared.SubmitForm.FillSubmitFormTextBox("edited");

            Assert.True(Components.Shared.SubmitForm.ErrorIsNotVisible());
        }
    }
}
