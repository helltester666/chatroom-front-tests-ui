﻿using Chatroom.Tests.UI.PageComponents.ChatRoomPage;
using Chatroom.Tests.UI.PageComponents.Shared;

namespace Chatroom.Tests.UI.PageComponents
{
    public class Components
    {
        public static SharedComponents Shared;

        // Page components
        public static MessageFeed ChatFeed = new MessageFeed();

        public static void Init()
        {
            Shared = new SharedComponents();
        }
    }
}
