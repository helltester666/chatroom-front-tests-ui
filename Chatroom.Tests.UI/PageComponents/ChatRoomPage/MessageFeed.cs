﻿using Chatroom.Tests.UI.Extensions;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace Chatroom.Tests.UI.PageComponents.ChatRoomPage
{
    public class MessageFeed
    {
        public MessageFeedElements Elements;

        public MessageFeed()
        {
            Elements = new MessageFeedElements();
        }

        public bool IsMessagePresent(string text)
        {
            return Driver.Wait.Until(d =>
                Elements.Messages.Any(m =>
                    m.MessageText.Text.Contains(text)));
        }
    }

    public class MessageFeedElements
    {
        private static Elements MessageWrappers => Driver.FindElements(By.CssSelector(".messageWrapper"), "Chat Message Wrapper");

        public List<MessageElements> Messages => MessageWrappers.Current.Select(messageWrapper => new MessageElements(messageWrapper)).ToList();
    }

    public class MessageElements
    {
        private readonly Element _parent;
        public Element MessageTime => Driver.FindElement(By.CssSelector(".sendTime"), "Message Time", _parent);
        public Element MessageSender => Driver.FindElement(By.CssSelector(".senderName"), "Message Sender", _parent);
        public Element MessageText => Driver.FindElement(By.CssSelector(".text"), "Message Text", _parent);

        public MessageElements(Element messageWrapper)
        {
            _parent = messageWrapper;
        }
    }
}