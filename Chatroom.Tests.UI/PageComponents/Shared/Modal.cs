﻿using Chatroom.Tests.UI.Extensions;
using OpenQA.Selenium;

namespace Chatroom.Tests.UI.PageComponents.Shared
{
    public class Modal
    {
        public ModalElements Elements;

        public Modal()
        {
            Elements = new ModalElements();
        }

        public bool IsJoinRoomModalVisible()
        {
            Driver.Wait.Until(d => Elements.ModalWrapper.Displayed);
            return Elements.ModalWrapper.Displayed &&
                   Elements.MessageLabel.Text.Contains("To join chat follow the link:") &&
                   Elements.JoinLink.Text.Contains("https://chatroom-front.herokuapp.com/?id=");
        }

        public bool IsDeleteChatModalVisible()
        {
            Driver.Wait.Until(d => Elements.ModalWrapper.Displayed);
            return Elements.ModalWrapper.Displayed &&
                   Elements.MessageLabel.Text.Contains("This chat has ended. You can delete it") &&
                   Elements.DeleteButton.Displayed;
        }

        public void DeleteChat()
        {
            Driver.Wait.Until(d => Elements.ModalWrapper.Displayed);
            Elements.DeleteButton.Click();
        }
    }

    public class ModalElements
    {
        public Element ModalWrapper => Driver.FindElement(By.CssSelector("#join, #delete, #modal"), "Modal window wrapper");
        public Element DeleteButton => Driver.FindElement(By.CssSelector("button:nth-child(1)"), "Delete Chat Button", ModalWrapper);
        public Element MessageLabel => Driver.FindElement(By.CssSelector("p"), "Modal window message", ModalWrapper);
        public Element JoinLink => Driver.FindElement(By.CssSelector("a"), "Modal window join link", ModalWrapper);
    }
}
