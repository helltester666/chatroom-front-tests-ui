﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Helpers;
using OpenQA.Selenium;

namespace Chatroom.Tests.UI.PageComponents.Shared
{
    public class Title
    {
        public TitleElements Elements;

        public Title()
        {
            Elements = new TitleElements();
        }

        public bool WaitForContainsText(string text)
        {
            FW.Log.Step($"Wait for page header contains {text}");
            return Driver.Wait.Until(d => Elements.PageTitle.Text.Contains(text));
        }
    }

    public class TitleElements
    {
        public Element PageTitle => Driver.FindElement(By.CssSelector("h1"), "Page title");
    }
}