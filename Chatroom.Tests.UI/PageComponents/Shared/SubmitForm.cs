﻿using Chatroom.Tests.UI.Extensions;
using Chatroom.Tests.UI.Models;
using OpenQA.Selenium;
using System.Threading;

namespace Chatroom.Tests.UI.PageComponents.Shared
{
    public class SubmitForm
    {
        public SubmitFormElements Elements;

        public SubmitForm()
        {
            Elements = new SubmitFormElements();
        }

        public void CreateChatRoom(ChatRoom chatroom)
        {
            FillSubmitFormTextBox(chatroom.Name);
            Elements.SubmitButton.Click();
        }

        public bool ErrorIsVisible()
        {
            FW.Log.Step("Wait for error is visible");
            return Driver.Wait.Until(d => Elements.ErrorWrapper.Displayed &&
                   Elements.ErrorText.Displayed &&
                   Elements.ErrorCloseButton.Displayed);
        }

        public void CloseMessage()
        {
            Driver.Wait.Until(d => Elements.ErrorWrapper.Displayed);
            Elements.ErrorCloseButton.Click();
        }

        public bool ErrorIsNotVisible()
        {
            FW.Log.Step("Wait for error is not visible");
            return Driver.Wait.Until(d => !Elements.ErrorWrapper.Displayed &&
                   !Elements.ErrorText.Displayed &&
                   !Elements.ErrorCloseButton.Displayed);
        }

        public void JoinChat(User user)
        {
            FillSubmitFormTextBox(user.Name);
            Elements.SubmitButton.Click();
        }

        public void SendMessage(string message)
        {
            FillSubmitFormTextBox(message);
            Elements.SubmitButton.Click();
        }

        public void FillSubmitFormTextBox(string text)
        {
            Driver.Wait.Until(d => Elements.InputTextBox.Displayed && Elements.InputTextBox.Enabled);

            // Wait for websocket is connected
            Thread.Sleep(1000);

            Elements.InputTextBox.SendKeys(text);
        }
    }

    public class SubmitFormElements
    {
        public Element InputTextBox => Driver.FindElement(By.CssSelector("#chat-room-name, #name, #text"), "Input TextBox");
        public Element SubmitButton => Driver.FindElement(By.CssSelector("button[type='submit']"), "Submit Button");
        public Element ErrorWrapper => Driver.FindElement(By.Id("error"), "Error Wrapper");
        public Element ErrorText => Driver.FindElement(By.CssSelector("p"), "Error Label", ErrorWrapper);
        public Element ErrorCloseButton => Driver.FindElement(By.CssSelector("button"), "Error Close Button", ErrorWrapper);

    }
}
