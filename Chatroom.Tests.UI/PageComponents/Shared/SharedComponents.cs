﻿namespace Chatroom.Tests.UI.PageComponents.Shared
{
    public class SharedComponents
    {
        public SubmitForm SubmitForm;
        public Modal Modal;
        public Title Title;

        public SharedComponents()
        {
            SubmitForm = new SubmitForm();
            Modal = new Modal();
            Title = new Title();
        }
    }
}
