﻿using System;

namespace Chatroom.Tests.UI.Models
{
    public class Message
    {
        public int ChatId { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public string Username { get; set; }

        public Message(ChatRoom chat, User user, string text)
        {
            ChatId = chat.Id;
            Date = DateTime.Now;
            Text = text;
            Username = user.Name;
        }
    }
}
