﻿using Newtonsoft.Json;

namespace Chatroom.Tests.UI.Models
{
    public class Connection
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("uuid")]
        public string Uuid { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        public Connection(User user, ChatRoom chat)
        {
            Id = chat.Id;
            Name = chat.Name;
            Status = chat.Status;
            Uuid = chat.Uuid;
            Username = user.Name;
        }
    }
}
