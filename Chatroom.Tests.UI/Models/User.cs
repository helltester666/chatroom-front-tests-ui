﻿using System.Collections.Generic;

namespace Chatroom.Tests.UI.Models
{
    public class User
    {
        public string Name;
        public List<Connection> Connections = new List<Connection>();

        public User(string name)
        {
            Name = name;
        }

        public void AddConnection(ChatRoom chat)
        {
            var connection = new Connection(this, chat);

            Connections.Add(connection);
        }

        public User() : this("") { }
    }
}
