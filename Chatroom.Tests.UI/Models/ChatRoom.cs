﻿namespace Chatroom.Tests.UI.Models
{
    public class ChatRoom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Uuid { get; set; }

        public ChatRoom(string name = "")
        {
            Name = name;
        }

        public ChatRoom() : this("") { }
    }
}
