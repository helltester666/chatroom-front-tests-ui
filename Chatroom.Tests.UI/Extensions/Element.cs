﻿using Chatroom.Tests.UI.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Drawing;

namespace Chatroom.Tests.UI.Extensions
{
    public class Element : IWebElement
    {
        private readonly IWebElement _element;

        public readonly string Name;

        public By FoundBy { get; set; }

        public IWebElement Current => _element ?? throw new NullReferenceException("_element not initialized");

        public Element(IWebElement element, string name)
        {
            _element = element;
            Name = name;
        }

        public IWebElement FindElement(By @by)
        {
            return Current.FindElement(@by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By @by)
        {
            return Current.FindElements(@by);
        }

        public void Clear()
        {
            FW.Log.Step($"Clear {Name}");
            Current.Clear();
        }

        public void SendKeys(string text)
        {
            FW.Log.Step($"Input text {text} to element {Name}");
            Current.SendKeys(text);
        }

        public void Submit()
        {
            FW.Log.Step($"Submit {Name}");
            Current.Submit();
        }

        public void Click()
        {
            FW.Log.Step($"Click {Name}");
            Current.Click();
        }

        public string GetAttribute(string attributeName)
        {
            FW.Log.Step($"Get {attributeName} for {Name}");
            return Current.GetAttribute(attributeName);
        }

        public string GetProperty(string propertyName)
        {
            FW.Log.Step($"Get {propertyName} for {Name}");
            return Current.GetProperty(propertyName);
        }

        public string GetCssValue(string propertyName)
        {
            FW.Log.Step($"Get {propertyName} for {Name}");
            return Current.GetCssValue(propertyName);
        }

        public string TagName => Current.TagName;
        public string Text => Current.Text;
        public bool Enabled => Current.Enabled;
        public bool Selected => Current.Selected;
        public Point Location => Current.Location;
        public Size Size => Current.Size;
        public bool Displayed => Current.Displayed;
    }
}
