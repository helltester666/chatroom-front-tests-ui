﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;

namespace Chatroom.Tests.UI.Extensions
{
    public static class Driver
    {
        [ThreadStatic]
        private static IWebDriver _driver;

        [ThreadStatic] public static Wait Wait;

        public static void Init()
        {
            FW.Log.Info("Browser: Chrome");
            _driver = new ChromeDriver();
            Wait = new Wait(FW.Config.WaitTimeout);
        }

        public static IWebDriver Instance => _driver ?? throw new NullReferenceException("Initialize driver first");

        public static void Goto(string url)
        {
            if (!url.StartsWith("http"))
            {
                url = $"http://{url}";
            }

            FW.Log.Info($"Getting url: {url}");
            Instance.Navigate().GoToUrl(url);
        }

        public static string Title()
        {
            FW.Log.Step("Get Page Title");
            return Instance.Title;
        }

        public static Element FindElement(By by, string elementName, Element parent = null)
        {
            var element = parent == null ? Instance.FindElement(by) : parent.Current.FindElement(by);

            return new Element(element, elementName)
            {
                FoundBy = by
            };
        }

        public static Elements FindElements(By by, string elementName, Element parent = null)
        {
            var elements = parent == null ? Instance.FindElements(by) : parent.Current.FindElements(by);

            return new Elements(elements, elementName)
            {
                FoundBy = by
            };
        }

        public static void Quit()
        {
            FW.Log.Info("Close Browser");
            Instance.Close();
            Instance.Quit();
            Instance.Dispose();
        }

        public static void TakeScreenshot(string imageName)
        {
            var ss = ((ITakesScreenshot)Instance).GetScreenshot();
            var fileName = Path.Combine(FW.CurrentTestDirectory.FullName, imageName);
            ss.SaveAsFile($"{fileName}.png", ScreenshotImageFormat.Png);
        }
    }
}
