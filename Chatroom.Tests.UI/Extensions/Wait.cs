﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Chatroom.Tests.UI.Extensions
{
    public class Wait
    {
        private readonly WebDriverWait _wait;

        public Wait(int waitSeconds)
        {
            _wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(waitSeconds));

            _wait.IgnoreExceptionTypes(
                typeof(NoSuchElementException),
                typeof(ElementNotVisibleException),
                typeof(StaleElementReferenceException)
            );
        }

        public bool Until(Func<IWebDriver, bool> condition)
        {
            return _wait.Until(condition);
        }
    }
}
