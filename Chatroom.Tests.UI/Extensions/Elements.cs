﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Chatroom.Tests.UI.Extensions
{
    public class Elements : ReadOnlyCollection<IWebElement>
    {
        private readonly IList<Element> _elements = new List<Element>();
        private readonly string _name;

        public By FoundBy { get; set; }

        public bool IsEmpty => Count == 0;

        public IList<Element> Current => _elements ?? throw new NullReferenceException("_elements not initialized");

        public Elements(IList<IWebElement> list, string name) : base(list)
        {
            foreach (var element in list)
            {
                _elements.Add(new Element(element, $"{name} [{_elements.Count + 1}]"));
            }

            _name = name;
        }
    }
}
