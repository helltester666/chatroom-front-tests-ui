﻿using System;

namespace Chatroom.Tests.UI.Helpers
{
    public static class TextGenerator
    {
        private static string[] _firstNames = { "John", "Sarah", "Mary", "Jack", "Donald", "Brad", "Chester", "Spencer", "Angela", "Sherilynn" };

        private static string[] _lastNames =
            {"Snow", "Connor", "Poppins", "Bowie", "Trump", "Pitt", "Bennington", "Sotelo", "Lansberry", "Fenn"};

        public static string GetRandomUuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static string GetRandomName()
        {
            var rand = new Random();
            var first = _firstNames[rand.Next(_firstNames.Length - 1)];
            var last = _lastNames[rand.Next(_lastNames.Length - 1)];
            return $"{first} {last}";
        }
    }
}
